import {createRouter, createWebHistory} from "vue-router"
import {useUserStore} from './stores/user'



const routes =[
    {
        path: '/',
        name: 'home',
        component: () => import('./views/HomePage.vue'),
        meta: {title: 'Список дел'},
        props: true,
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('./views/LoginPage.vue'),
        meta: {title: 'Вход'},
        props: true,
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('./views/RegisterPage.vue'),
        meta: {title: 'Регистрация'},
        props: true,
    },
    {
        path: '/create',
        name: 'create',
        component: () => import('./views/CreateTask.vue'),
        meta: {title: 'Создать'},
        props: true,
    },
    {
        path: '/ItemDetail/:id',
        name: 'ItemDetail',
        component: () => import('./views/ItemDetail.vue'),
        meta: {title: 'Задача'},
        props:true,
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: () => import('./views/NotFound.vue'),
        meta: {title: 'NotFound'},
        props: true,
    },
]


const router = createRouter(  {
    routes,
    history: createWebHistory(),
})


router.beforeEach(function(to,from,next){
    document.title = to.meta.title || 'To do vue app'

    const userStore = useUserStore()
    if (!userStore.token && to.name !=='login')
        next({name: 'login'})
    else
        next()
 }
)


export default router