import { defineStore } from 'pinia'

export const useTodoStore = defineStore("todo", {
  state(){
    return {
     tasks:[]
    }
  },
  getters: {
    getTask(state){
      return state.tasks
    }
  },
  actions:{
    async load(){
      const response = await this.$axios.get('/items/')
      this.tasks = response.data
    },
    addNewTask(name) {
      this.tasks.push({
      id: this.tasks.length + 1,
      name: name,
      done: false,
      })
    },
    editTask(){
      this.tasks.push()
    },
    removeTask(task) {
      this.tasks.splice(task, 1);
      console.log(this.tasks.slice(task, 1));
    },
  }
})