import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', {
  state() {
    return {
     _token: undefined
    }
  },
  getters:{
    token(state){
      if (!state._token){
        state._token = localStorage.getItem('token', this._token)
      }
      return state._token
    }
  },
  actions:{
    async register(username, password){
      let {isSuccess,result} = await this.makeRequest({
        method:'post',
        url: '/user/register/',
        data: {username, password},
      })
      return {isSuccess,result}
      },
    async login(username, password){
      let {isSuccess,result} = await this.makeRequest({
        method:'post',
        url: '/user/login/',
        data: {username, password}
      })

      if (isSuccess){
        this._token = result.token
        localStorage.setItem('token', this._token)
      }
        return {isSuccess,result}
      
    },
    logout(){
      this._token = undefined
      localStorage.removeItem('token')
    }
  }
})
