import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import ElementPlus from 'element-plus'
import ru from 'element-plus/dist/locale/ru.mjs'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import router from './router'
import { axiosPiniaPlugin } from './stores/axiosPlugin'

import 'element-plus/dist/index.css'
import './main.css'

const pinia = createPinia()
pinia.use(axiosPiniaPlugin)
const app =createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

app.use(ElementPlus,{locale: ru})
    .use(pinia)
    .use(router)
    .mount('#app')